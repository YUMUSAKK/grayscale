namespace WindowsFormsApplication5
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.dosyaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.açToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kaydetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.islemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ortalamaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bt709AlgoritmasiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lumaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.acıklıkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kanalCıkarımıToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kaynakbox = new System.Windows.Forms.PictureBox();
            this.islembox = new System.Windows.Forms.PictureBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kaynakbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.islembox)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Location = new System.Drawing.Point(0, 24);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(883, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuStrip2
            // 
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dosyaToolStripMenuItem,
            this.islemToolStripMenuItem});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(883, 24);
            this.menuStrip2.TabIndex = 1;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // dosyaToolStripMenuItem
            // 
            this.dosyaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.açToolStripMenuItem,
            this.kaydetToolStripMenuItem});
            this.dosyaToolStripMenuItem.Name = "dosyaToolStripMenuItem";
            this.dosyaToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.dosyaToolStripMenuItem.Text = "Dosya";
            // 
            // açToolStripMenuItem
            // 
            this.açToolStripMenuItem.Name = "açToolStripMenuItem";
            this.açToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.açToolStripMenuItem.Text = "aç";
            this.açToolStripMenuItem.Click += new System.EventHandler(this.açToolStripMenuItem_Click);
            // 
            // kaydetToolStripMenuItem
            // 
            this.kaydetToolStripMenuItem.Name = "kaydetToolStripMenuItem";
            this.kaydetToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.kaydetToolStripMenuItem.Text = "kaydet";
            this.kaydetToolStripMenuItem.Click += new System.EventHandler(this.kaydetToolStripMenuItem_Click);
            // 
            // islemToolStripMenuItem
            // 
            this.islemToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ortalamaToolStripMenuItem,
            this.bt709AlgoritmasiToolStripMenuItem,
            this.lumaToolStripMenuItem,
            this.acıklıkToolStripMenuItem,
            this.kanalCıkarımıToolStripMenuItem});
            this.islemToolStripMenuItem.Name = "islemToolStripMenuItem";
            this.islemToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.islemToolStripMenuItem.Text = "islem";
            this.islemToolStripMenuItem.Click += new System.EventHandler(this.islemToolStripMenuItem_Click);
            // 
            // ortalamaToolStripMenuItem
            // 
            this.ortalamaToolStripMenuItem.Name = "ortalamaToolStripMenuItem";
            this.ortalamaToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.ortalamaToolStripMenuItem.Text = "ortalama";
            this.ortalamaToolStripMenuItem.Click += new System.EventHandler(this.ortalamaToolStripMenuItem_Click);
            // 
            // bt709AlgoritmasiToolStripMenuItem
            // 
            this.bt709AlgoritmasiToolStripMenuItem.Name = "bt709AlgoritmasiToolStripMenuItem";
            this.bt709AlgoritmasiToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.bt709AlgoritmasiToolStripMenuItem.Text = "bt-709 algoritmasi";
            this.bt709AlgoritmasiToolStripMenuItem.Click += new System.EventHandler(this.bt709AlgoritmasiToolStripMenuItem_Click);
            // 
            // lumaToolStripMenuItem
            // 
            this.lumaToolStripMenuItem.Name = "lumaToolStripMenuItem";
            this.lumaToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.lumaToolStripMenuItem.Text = "luma";
            this.lumaToolStripMenuItem.Click += new System.EventHandler(this.lumaToolStripMenuItem_Click);
            // 
            // acıklıkToolStripMenuItem
            // 
            this.acıklıkToolStripMenuItem.Name = "acıklıkToolStripMenuItem";
            this.acıklıkToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.acıklıkToolStripMenuItem.Text = "acıklık";
            // 
            // kanalCıkarımıToolStripMenuItem
            // 
            this.kanalCıkarımıToolStripMenuItem.Name = "kanalCıkarımıToolStripMenuItem";
            this.kanalCıkarımıToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.kanalCıkarımıToolStripMenuItem.Text = "kanal cıkarımı";
            this.kanalCıkarımıToolStripMenuItem.Click += new System.EventHandler(this.kanalCıkarımıToolStripMenuItem_Click);
            // 
            // kaynakbox
            // 
            this.kaynakbox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.kaynakbox.Location = new System.Drawing.Point(0, 37);
            this.kaynakbox.Name = "kaynakbox";
            this.kaynakbox.Size = new System.Drawing.Size(400, 400);
            this.kaynakbox.TabIndex = 2;
            this.kaynakbox.TabStop = false;
            this.kaynakbox.Click += new System.EventHandler(this.kaynakbox_Click);
            // 
            // islembox
            // 
            this.islembox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.islembox.Location = new System.Drawing.Point(448, 37);
            this.islembox.Name = "islembox";
            this.islembox.Size = new System.Drawing.Size(400, 400);
            this.islembox.TabIndex = 3;
            this.islembox.TabStop = false;
            this.islembox.Click += new System.EventHandler(this.islembox_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(883, 516);
            this.Controls.Add(this.islembox);
            this.Controls.Add(this.kaynakbox);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.menuStrip2);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kaynakbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.islembox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem dosyaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem açToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kaydetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem islemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bt709AlgoritmasiToolStripMenuItem;
        private System.Windows.Forms.PictureBox kaynakbox;
        private System.Windows.Forms.PictureBox islembox;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem kanalCıkarımıToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem acıklıkToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lumaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ortalamaToolStripMenuItem;
    }
}

